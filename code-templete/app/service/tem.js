'use strict';

const tableName ="auth_tb_user";
module.exports = app => {
  class TemService extends app.Service {       
    * getList(option){  
               
        const defaultOption= {
              where: {},             
             // orders: [['create_datetime', 'desc'], ['id', 'desc']],
              limit: 2,
              offset:0
          };
        if (option.hasOwnProperty("pageSize")) {
            defaultOption.limit = parseInt(option.pageSize);
        }

        if (option.hasOwnProperty("pageNum")){
            let npage=parseInt(option.pageNum);
            if (npage>1){
                defaultOption.offset = (npage-1) * defaultOption.limit;
            }
              
        }
        Object.assign(defaultOption, option);  
        console.log(defaultOption);
        
        const results = yield app.mysql.select(tableName, defaultOption);
        //const string = yield app.mysql.selectToString(tableName, defaultOption);
        
        const counts = yield app.mysql.count(tableName, defaultOption.where);
        return { string:string,records: results, counts: counts, pageSize: defaultOption.limit, pageNum: defaultOption.offset};
    }

    * getOneByOption(option){       
         const result = yield app.mysql.get(tableName, option);
         return result;
    }
    
    * add(option){
        const result = yield app.mysql.insert(tableName, option);
        const insertSuccess = result.affectedRows === 1;
        return insertSuccess;
    }

    * delete(option){
        const result = yield app.mysql.delete(tableName, option);
        const delSuccess = result.affectedRows === 1;
        return delSuccess;
    }

    * update(row){
        const result = yield app.mysql.update(tableName, row);
        const updateSuccess = result.affectedRows === 1;
        return updateSuccess;
    }

  }
  return TemService;
};
