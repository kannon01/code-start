'use strict';

module.exports = app => {
  class TemController extends app.Controller {    
    * create(){
      const ctx = this.ctx; 
      ctx.logger.info(ctx.request.body);     
      try{
        const rs = yield ctx.service.tem.add(ctx.request.body);
        this.success({ rs });
      }catch(err){ 
        ctx.logger.error(new Error(err));         
        this.error({error:err.errno,msg:err.sqlMessage});      
      }
    }
    * show(){
      const ctx = this.ctx;
      const params = ctx.params;   
      ctx.logger.info(params);
      try {
        const record = yield ctx.service.tem.getOneByOption({ id: params.id });
        this.success(record);
      } catch (err) {
        ctx.logger.error(new Error(err));
        this.error({ error: err.errno, msg: err.sqlMessage });
      }
    }

    * update(){
      const ctx = this.ctx;
      const body = ctx.request.body;  
      const params = ctx.params;      
      const option = {};
      Object.assign(option,params,body);        
      ctx.logger.info(option);
      try {
        const records = yield ctx.service.tem.update(option);
        this.success({ records});
      } catch (err) {
        ctx.logger.error(new Error(err));
        this.error({ error: err.errno, msg: err.sqlMessage });
      }
    }

    * index(){
      const ctx = this.ctx;
      const query = ctx.request.query;
      const rule={
        pageNum:{
          type: "integer",
          required:false
        },
        pageSize: {
          type: "integer",
          required: false
        }        
      };
      ctx.validate(rule, query);
      ctx.logger.info(query);
      try {
        const records = yield ctx.service.tem.getList(query);
        this.success(records);
      } catch (err) {
        ctx.logger.error(new Error(err));
        this.error({ error: err.errno, msg: err.sqlMessage });
      }
    }
    * destroy(){
      const ctx = this.ctx;    
      const params = ctx.params;
      ctx.logger.info(params);    
      try {
        const records = yield ctx.service.tem.delete({ id: params.id});
        this.success({ records, total: 5, current: 1 });
      } catch (err) {
        ctx.logger.error(new Error(err));
        this.error({ error: err.errno, msg: err.sqlMessage });
      }
    }
  }
  return TemController;
};

