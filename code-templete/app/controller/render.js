'use strict';

module.exports = app => {
    class RenderController extends app.Controller{
        * index(){  
            const { ctx, service } = this;
            const argesRule = {
                templete: { type: 'string' },
                data: { type: 'string' },
            };
            // 校验参数
            ctx.validate(argesRule, ctx.query);
            // 组装参数
            const author = ctx.session.userId;
            const req = Object.assign(ctx.query, { author });

            //const data = { name: 'egg' };       
            ctx.body = req;  
            //ctx.body = yield ctx.renderView('path/to/file.tpl', data);          
        }
    }
    return RenderController;
};