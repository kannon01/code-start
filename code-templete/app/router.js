'use strict';

module.exports = app => {
  app.get('/', 'home.index');
  app.get('/render', app.controller.render.index);
  app.post('/upload', app.controller.upLoad.upload);
  app.get('/upPage', app.controller.upLoad.page);
  
 
  // app.get('/test/add', app.controller.tem.add);
  // app.get('/test/update', app.controller.tem.update);
  // app.get('/test/del', app.controller.tem.delete);

  // app.get('/authTbUser/get', app.controller.tem.index);
  // app.get('/authTbUser/:id/detail', app.controller.tem.detail);

  app.resources('tem', '/api/v2/authTbUser', 'tem');
};
