'use strict';

const path = require('path');

module.exports = appInfo => {
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1506254589024_6174';

  // add your config here
  config.middleware = [];
  config.view={
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.nj': 'nunjucks',
    },
    root: [
      path.join(appInfo.baseDir, 'app/view'),
      path.join(appInfo.baseDir, 'path/to/another'),
    ].join(','),
  };
//GdKGlHPPlylH9BLalgfSifbDmuZldY 
//LTAI0nOG5lawrWQ7
  config.oss = {
    client: {
      accessKeyId: 'LTAIdB0nB0JG20Hh',
      accessKeySecret: 'SY26div7MGdQUzgLLUa0i5iwnXyB1E',
      bucket: 'static-io',
      region: 'oss-cn-beijing',
      endpoint: 'oss-cn-beijing.aliyuncs.com',
      timeout: '60s',
    },
  };

  config.mysql = {
    // database configuration
    client: {
      // host
      host: 'www.imshow.vip',
      // port
      port: '3306',
      // username
      user: 'db_pro',
      // password
      password: 'Wyd1313123...',
      // database
      database: 'db_pro',    
    },
    // load into app, default is open
    app: true,
    // load into agent, default is close
    agent: false,
  };


  config.logger = {
    dir: path.join(appInfo.baseDir, 'logs'),
    level: 'DEBUG',
  };

  config.security = {
    csrf: {
      ignoreJSON: true, // 默认为 false，当设置为 true 时，将会放过所有 content-type 为 `application/json` 的请求
    },
    methodnoallow: {
      enable: false
    }, 
    domainWhiteList: [
      'http://localhost',
      'http://120.0.0.1',
      'http://localhost:8000',
    ]
  };

  config.cors = {
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH',
    credentials: true
  }
    
  return config;
};
