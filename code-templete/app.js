module.exports = app => {
    class CustomController extends app.Controller {
      get user() {
        return this.ctx.session.user;
      }
      success(data) {
        this.ctx.body = {
          state: true,
          data,
        };
      }

      error({msg,err}) {
        this.ctx.body = {
          state: false,
          msg,
          err,
        };
      }
      notFound(msg) {
        msg = msg || 'not found';
        this.ctx.throw(404, msg);
      }
    }
    app.Controller = CustomController;
    app.validator.addRule('json', (rule, value) => {
        try {
          JSON.parse(value);
        } catch (err) {
          return 'must be json string';
        }
      });

    app.validator.addRule('integer', (rule, value) => {
      try {      
        parseInt(value);
      } catch (err) {
        return 'must be integer';
      }
    });


    const client=app.mysql;

    client.selectToString = function* (table, options) {
      options = options || {};
      const sql = this._selectColumns(table, options.columns) +
        this._where(options.where) +
        this._orders(options.orders) +
        this._limit(options.limit, options.offset);
      return sql;
    };

    client.selectMultTable = function* () {
      
    }
    
  }