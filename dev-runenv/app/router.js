'use strict';

module.exports = app => {
  app.get('/', 'home.index');
  app.post('/gitwebhook','webhook.webhook');   
  
  app.get('/user/login', 'user.login');
};
