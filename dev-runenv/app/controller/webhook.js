'use strict';
module.exports = app => {
    class WebHookController extends app.Controller{
        * webhook(){   
            console.log('webhook start');
            console.log(this.ctx.request.body);            
            const query = this.ctx.query;           
            yield this.ctx.service.shell.exec(query.name);             
            this.ctx.body={query,body};           
        }
    }
    return WebHookController;
};