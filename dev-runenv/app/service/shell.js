'use strict';
const fs = require('fs');
module.exports = app => {
  class ShellService extends app.Service {
    * exec(name){       
        let exec = require('child_process').exec;
        let cmdStr = fs.readFileSync(app.baseDir+'/app/shell/'+name+'.sh','utf-8');       
        exec(cmdStr, function (err, stdout, stderr) {
            if (err) {
                console.log('get weather api error:' + stderr);
            } else {               
                console.log(stdout);
            }
        });
    }
  }
  return ShellService;
};
