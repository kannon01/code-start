'use strict';

let path = require('path')

module.exports = appInfo => {
  const config = exports = {
    
  };

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1506436580865_9500';

  // add your config here
  config.middleware = [

  ];

  config.jsonp = {
    csrf: true,
    whiteList: ['.localhost*', '.120.0.0.1*',".DESKTOP-469MECT*"],
    callback: 'callback', // 识别 query 中的 `callback` 参数
    limit: 100, // 函数名最长为 100 个字符
  }

  config.security = {
    csrf: {
      ignoreJSON: true, // 默认为 false，当设置为 true 时，将会放过所有 content-type 为 `application/json` 的请求
    },    
    domainWhiteList: [
      'localhost',
      'http://localhost:8000',
    ]
  };

  config.cors = {
    allowMethods: 'GET',
    credentials: true
  }

  config.static = {   
    prefix: '/shell/',
    dir: path.join(appInfo.baseDir, 'app/shell'),
    dynamic: true,
    preload: false,
    maxAge: 31536000, 
    buffer: true,
  }

 
 

  return config;
};
