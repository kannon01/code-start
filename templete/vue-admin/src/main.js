// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
import router from './router'
import sideMenu from './components/SideMenu.vue'
import headerMenu from './components/HeaderMenu.vue'

Vue.use(ElementUI)
Vue.config.productionTip = false

Vue.component('w-sideMenu', sideMenu)
Vue.component('w-headerMenu', headerMenu)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
