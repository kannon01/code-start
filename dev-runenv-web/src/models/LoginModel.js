import * as userService from '../services/user/login';


export default {
  namespace: 'LoginModel',
  state: {
    error:{
      code :null,
      msg :null
    }
  },
  reducers: {

  },
  effects: {
    * login({payload,},{call,put,select}){
        console.log(payload);
        const reData=yield call(userService.login,payload);
        console.log(reData);
    }
  },
  subscriptions: {

    
  },
};
