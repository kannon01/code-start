import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route, Redirect, routerRedux } from 'dva/router'
import dynamic from 'dva/dynamic'

const { ConnectedRouter } = routerRedux

const Routers = function ({ history, app }) {
  const error = dynamic({
    app,
    // component: () => import('./routes/LoginPage'),
  })
  const routes = [
   {
      path: '/',
      models: () => [import('./models/LoginModel')],
      component: () => import('./routes/LoginPage'),
    },
  ]

  return (
    <ConnectedRouter history={history}>     
        <Switch>
          {/* <Route exact path="/" render={() => (<Redirect to="/login" />)} /> */}
          {
            routes.map(({ path, ...dynamics }, key) => (
              <Route key={key}
                exact
                path={path}
                component={dynamic({
                  app,
                  ...dynamics,
                })}
              />
            ))
          }
          {/* <Route component={error} /> */}
        </Switch>    
    </ConnectedRouter>
  )
}

Routers.propTypes = {
  history: PropTypes.object,
  app: PropTypes.object,
}

export default Routers
