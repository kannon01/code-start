import React from 'react';
import { connect } from 'dva'
import PropTypes from 'prop-types';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Row, Col } from 'antd';
import styles from './LoginPage.css';

const FormItem = Form.Item;

class NormalLoginForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);  
        console.log( this.props.dispatch);        
        this.props.dispatch({ type: 'LoginModel/login', payload: values })
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row>
        <Col md={{span:6,offset:9}} style={{marginTop:100}}>
          <Form onSubmit={this.handleSubmit} className={styles.loginForm}>
            <FormItem>
              <div className={styles.loginFormTitle}>CODE-SHELL</div>
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(                
                <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
              )}             
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(
                <Checkbox>Remember me</Checkbox>
              )}
              <a className={styles.loginFormForgot} href="">Forgot password</a>
              <Button type="primary" htmlType="submit" className={styles.loginFormButton}>
                Log in
              </Button>            
              Or <a href="">register now!</a>
            </FormItem>
          </Form>
        </Col>      
      </Row>
     
    );
  }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);

WrappedNormalLoginForm.propTypes = {
  form: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ loading }) => ({ loading }))(Form.create()(WrappedNormalLoginForm))

